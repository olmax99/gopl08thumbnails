// Copyright © 2016 Alan A. A. Donovan & Brian W. Kernighan.
// License: https://creativecommons.org/licenses/by-nc-sa/4.0/

// The thumbnail command produces thumbnails of JPEG files
// whose names are provided on each line of the standard input.

package main

import (
	"fmt"
	"log"
	"os"
	"strings"
	"sync"
	"time"

	"gopl.io/ch8/thumbnail"
)

// MyError is an error implementation that includes a time and message.
type MyError struct {
	When time.Time
	What string
}

func (e MyError) Error() string {
	return fmt.Sprintf("%v: %v", e.What, e.When)
}

func corruptJpg() (string, error) {
	return "", MyError{
		time.Now(),
		"corrupted jpg ..",
	}
}

// Avoid local anonymous functions in concurrency design
func workThumbnails(file string, out chan<- int64, eout chan<- error, wg *sync.WaitGroup) {
	defer wg.Done()
	thumbfile, err := thumbnail.ImageFile(file)
	// thumbfile, err := corruptJpg()
	if err != nil {
		// log.Printf("[worker: makeThumbnails] %s: %s", file)
		eout <- err
		out <- 0
	} else {
		info, _ := os.Stat(thumbfile)
		// sizes SEND channel (needs closing)
		out <- info.Size() // SEND result to chan targetfiles
	}
}

// Fade Out goroutine - will start concurrent process for every input file
func makeThumbnails(filenames <-chan string, sizes chan<- int64, eout chan<- error) {
	var wg sync.WaitGroup
	// local RECEIVE
	for f := range filenames {
		wg.Add(1)
		go workThumbnails(f, sizes, eout, &wg)
	}

	// wg.Wait()
	// close(sizes)
	// 1. If the wait operation were placed before the sizes loop, it would never end???
	// >> SEND info.Size will be stuck as there is no receiver

	// NOTE: Must be concurrent with the loop over sizes - see 1. and 2.
	// closer - wait for goroutines to complete
	go func() {
		// Blocking until all thumbnail goroutines are finished
		wg.Wait()
		close(sizes)
	}()

	// 2. If the wait operation would be placed after the loop, the loop would never terminate???
	// >> sizes never closed (waiting for next receive is blocking)
	// wg.Wait()
	// close(sizes)
}

// Goroutine that is synchronized with the makeThumbnails task
// Needs to be concurrent, because makeThumbnail will process one file at a time (unbuffered)
// and can only proceed, when next RECEIVE call is being made
func transfer(out chan<- string, in []string) {
	for _, v := range in {
		if !strings.HasSuffix(v, ".jpg") {
			log.Fatalf("not jpg .. %s\n", v)
		}
		out <- v // targetfiles SEND
	}

	// Why does this not take effect immediately? .. this is a control signal
	// After the signal any RECEIVE will yield 0, false := <-c
	// SEND channels need closing when unbuffered
	close(out)
}

func calculateTotal(in <-chan int64, eout <-chan error) int64 {
	var total int64
	for info := range in {
		select {
		case x := <-eout:
			log.Fatalf("[worker: makeThumbnails] %s", x)
		default:
			total += info
		}
	}
	return total
}

//     <go transfer>            <go makeThumbnails>                <calculateTotal>
// --> (SEND) (close) filenames (RECEIVE) --> (SEND) (close) sizes (RECEIVE)
//     --provide next file--    --do work and provide file info--  --calculate total--
func main() {
	if len(os.Args) == 1 {
		log.Fatal("USAGE: thumbnails file1.jpg file2.jpg ...\n")
	}

	fmt.Printf("[+] .. make thumbnails from %q\n", os.Args[1:])

	targetfiles := make(chan string)
	fileinfos := make(chan int64)
	fails := make(chan error, 1) // first error will exit the program

	go transfer(targetfiles, os.Args[1:])
	go makeThumbnails(targetfiles, fileinfos, fails)

	p := calculateTotal(fileinfos, fails)
	fmt.Printf("processed: %v\n", p)

}
